﻿using JFCUtil.Game;
using UnityEngine;

namespace KeepYourHeadOn.Code
{
	public class KeepYourHeadOn : MonoBehaviour, IGameClass
	{	
		public CheckDistanceFromStart Eye;
		public CheckDistanceFromStart OtherEye;
		public CheckDistanceFromStart Nose;
		public CheckDistanceFromStart Mouth;
		private int _clampTop;

		public int GetScoreOutOfOneHundred()
		{
			_clampTop = 3;
			
			var eyeD = 		Mathf.Clamp(Eye.DistanceFromStart,		0,	_clampTop);
			var otherEyeD = Mathf.Clamp(OtherEye.DistanceFromStart,	0,	_clampTop);
			var noseD = 	Mathf.Clamp(Nose.DistanceFromStart,		0,	_clampTop);
			var mouthD = 	Mathf.Clamp(Mouth.DistanceFromStart,	0,	_clampTop);
			
			var aggregatedDistance = eyeD + otherEyeD + noseD + mouthD;

			var scoreUnits = 100f / (4f * _clampTop);
			var missedPoints = aggregatedDistance * scoreUnits;
			var scoreOutOfOneHundred = (int) (100f - missedPoints);
			
			return scoreOutOfOneHundred;
		}

		public void AffectScore(int affect)
		{
			throw new System.NotImplementedException();
		}
	}
}
