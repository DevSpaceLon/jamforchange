﻿using JFCUtil.Game;
using JFCUtil.Math;
using UnityEngine;
using UnityEngine.UI;

namespace FarFromDeerGame.Code
{
	public class FarFromDeerGame : MonoBehaviour, IGameClass
	{
		public Text TopText;
		public Text Players;
		
		public int GetScoreOutOfOneHundred()
		{
			var multiplier = 100 / TopText.text.Length;
			return 100 - LevenshteinDistance.Compute(TopText.text, Players.text) * multiplier;
		}

		public void AffectScore(int affect)
		{
			throw new System.NotImplementedException();
		}
	}
}
