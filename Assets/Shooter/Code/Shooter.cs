﻿using JFCUtil.Game;
using UnityEngine;

namespace Shooter.Code
{
	public class Shooter : MonoBehaviour, IGameClass 
	{
		private int _score;

		public int GetScoreOutOfOneHundred()
		{
			return _score;
		}

		public void AffectScore(int affect)
		{
			_score += affect;
		}
	}
}
