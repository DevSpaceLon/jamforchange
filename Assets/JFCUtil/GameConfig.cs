﻿using UnityEngine;

namespace JFCUtil
{	
	[CreateAssetMenu(fileName = "GameConfig", menuName = "JFCUtil/GameConfig")]
	public class GameConfig : ScriptableObject
	{
		[Range(1, 10)] 
		public float GameLength;
		
		public string GameName;

		public GameObject TitleObject;
		public GameObject GamePrefab;
		public GameObject ScorePrefab;

		public Color MainColor;
		public Color SecondaryColor;
		public Color TertiaryColor;
	}
}
