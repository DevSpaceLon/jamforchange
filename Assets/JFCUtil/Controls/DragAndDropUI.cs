﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JFCUtil.Controls
{
	// ReSharper disable once InconsistentNaming
	public class DragAndDropUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
	{
		private Vector3 _startPosition;
		private Vector3 _offsetToMouse;
		private float _zDistanceToCamera;
 
		#region Interface Implementations
 
		public void OnBeginDrag (PointerEventData eventData)
		{
			Debug.Log("OnBeginDrag");
			_startPosition = transform.position;
			_zDistanceToCamera = Mathf.Abs (_startPosition.z - Camera.main.transform.position.z);
 
			_offsetToMouse = _startPosition - Camera.main.ScreenToWorldPoint (
				                 new Vector3 (Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
			                 );
		}
 
		public void OnDrag (PointerEventData eventData)
		{
			Debug.Log("OnDrag");
			if(Input.touchCount > 1)
				return;
 
			transform.position = Camera.main.ScreenToWorldPoint (
				                     new Vector3 (Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
			                     ) + _offsetToMouse;
		}
 
		public void OnEndDrag (PointerEventData eventData)
		{
			Debug.Log("OnEndDrag");
			_offsetToMouse = Vector3.zero;
		}
 
		#endregion
	}
}