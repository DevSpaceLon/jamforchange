﻿using UnityEngine;

namespace JFCUtil.Controls
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class DragSprite : MonoBehaviour
	{
		private Rigidbody2D _body;
		private Vector3 _startPos;
		private Vector3 _newPos;

		private void OnMouseDown()
		{
			_startPos = transform.position;
		}

		private void OnMouseDrag()
		{
			_newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			_newPos.z = _startPos.z;
			transform.position = _newPos;
		}
	}
}
