﻿using UnityEngine;
using UnityEngine.UI;

namespace JFCUtil.Display
{
	[RequireComponent(typeof(Text))]
	public class SetTextRandomItem : MonoBehaviour
	{
		public bool BOnAwake;
		public string[] TextItems;

		private Text _text;
		private int _previous;

		private void Awake()
		{
			_text = GetComponent<Text>();
			
			if(BOnAwake)
				SetTextRandom();
		}

		// ReSharper disable once MemberCanBePrivate.Global
		public void SetTextRandom()
		{
			if (TextItems.Length <= 0) return;
			var index0 = _previous;

			while (index0 == _previous)
				index0 = Random.Range(0, TextItems.Length);
			
			_previous = index0;
			_text.text = TextItems[index0];
		}
	}
}
