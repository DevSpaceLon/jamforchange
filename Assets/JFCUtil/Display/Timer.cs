﻿using UnityEngine;
using UnityEngine.UI;

namespace JFCUtil.Display
{
	[RequireComponent(typeof(Text))]
	public class Timer : MonoBehaviour
	{
		private float _timeLeft;
		private Text _text;
		private float _startTime;
		private bool _set;

		public void SetTimer(float currentGameGameLength)
		{
			_startTime = _timeLeft = currentGameGameLength;
			_text = GetComponent<Text>();
			_set = true;
		}

		private void Update()
		{
			if (!_set)
				return;
			
			_timeLeft -= Time.deltaTime;
			_text.text = Mathf.Clamp(_timeLeft,0,_startTime).ToString("0.0");
		}
	}
}
