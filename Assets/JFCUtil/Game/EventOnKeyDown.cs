﻿using UnityEngine;
using UnityEngine.Events;

namespace JFCUtil.Game
{
	public class EventOnKeyDown : MonoBehaviour
	{
		public UnityEvent OnDown;
		public KeyCode KeyCode;
		
		// Update is called once per frame
		void Update ()
		{
			if(Input.GetKeyDown(KeyCode) && OnDown != null)
				OnDown.Invoke();
		}
	}
}
