﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class MoveToRandomScreenPos : MonoBehaviour
	{
		public bool BOnAwake;
		public Vector2 MaxHorizontals;
		public Vector2 MaxVerticals;

		public float MoveSpeed = 1f;
		
		private Vector2 _targetPos;
		private bool _bGotPosition;

		private void Awake()
		{
			if (BOnAwake)
				SelectRandomPos();
		}

		private void Update()
		{
			if (!_bGotPosition)
				return;
			
			if(Vector2.Distance(transform.position, _targetPos) < 0.25f)
				SelectRandomPos();
			else
				transform.position = Vector2.Lerp(transform.position, _targetPos, Time.deltaTime * MoveSpeed);
		}

		private void SelectRandomPos()
		{
			var x = Random.Range(MaxHorizontals.x, MaxHorizontals.y);
			var y = Random.Range(MaxVerticals.x, MaxVerticals.y);
			_targetPos = new Vector2(x, y);
			_bGotPosition = true;
		}
	}
}
