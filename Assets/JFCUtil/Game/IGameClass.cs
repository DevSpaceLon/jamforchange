﻿namespace JFCUtil.Game
{
	public interface IGameClass
	{
		int GetScoreOutOfOneHundred();
		void AffectScore(int affect);
	}
}
