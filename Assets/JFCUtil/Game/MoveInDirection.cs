﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class MoveInDirection : MonoBehaviour
	{
		public Vector2 Direction;
		public float Speed = 3f;
		
		// Update is called once per frame
		void Update () 
		{
			transform.Translate(Direction * Speed);
		}
	}
}
