﻿using UnityEngine;

namespace JFCUtil.Game
{
	public interface IScoreScreen
	{
		void SetScreen(Color p, Color s, Color t, int score);
	}
}
