﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class DieOffScreen : MonoBehaviour
	{
		public bool DestroyOnDeath;
		public float MaxInAnyDir = 10f;

		private void Update()
		{
			var x = transform.position.x < MaxInAnyDir;
			var nx = transform.position.x > -MaxInAnyDir;
			var y = transform.position.y < MaxInAnyDir;
			var ny = transform.position.y > -MaxInAnyDir;
			
			if (x && nx && y && ny)
				return;
			
			var deadables = GetComponents<IDeadable>();

			foreach (var deadable in deadables)
				deadable.Die();
			
			if(DestroyOnDeath)
				Destroy(gameObject);
		}
	}
}
