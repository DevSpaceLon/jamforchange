﻿using UnityEngine;
using UnityEngine.Events;

namespace JFCUtil.Game
{
	public class EventOnDeath : MonoBehaviour, IDeadable
	{
		public UnityEvent OnDead;

		public void Die()
		{
			if(OnDead != null)
				OnDead.Invoke();
		}
	}
}
