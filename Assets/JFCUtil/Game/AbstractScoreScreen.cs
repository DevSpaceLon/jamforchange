﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace JFCUtil.Game
{
	public abstract class AbstractScoreScreen : MonoBehaviour, IScoreScreen 
	{
		private Text _scoreText;
		private Text _text;
		private Image _bg;

		public void SetScreen(Color p, Color s, Color t, int score)
		{
			_scoreText = transform.Find("Score").GetComponent<Text>();
			_text = transform.Find("Text").GetComponent<Text>();
			_bg = transform.Find("BG").GetComponent<Image>();

			_scoreText.text = score.ToString();
			
			_scoreText.color = s;
			_text.color = t;
			_bg.color = p;
		}
	}
}
