﻿namespace JFCUtil.Game
{
	public interface IDeadable
	{
		void Die();
	}
}
