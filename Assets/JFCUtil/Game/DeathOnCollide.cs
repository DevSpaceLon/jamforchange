﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class DeathOnCollide : MonoBehaviour
	{
		public string Tag;
		
		private void OnCollisionEnter2D(Collision2D other)
		{
			var compareTag = other.gameObject.CompareTag(Tag);
			Debug.Log("Collide: " + compareTag.ToString());
			if(compareTag)
				GetComponent<IDeadable>().Die();
		}
	}
}
