﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class TranslateInDirection : MonoBehaviour
	{
		public Vector2 MoveVec;

		public void Translate()
		{
			transform.Translate(MoveVec);
		}
	}
}
