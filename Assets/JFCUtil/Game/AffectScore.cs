﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class AffectScore : MonoBehaviour
	{
		public MonoBehaviour GameClass;
		public int Effect;

		public void Affect()
		{
			var affectable = GameClass.GetComponent<IGameClass>();
			if(affectable != null)
				affectable.AffectScore(Effect);
		}

	}
}
