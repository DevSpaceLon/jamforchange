﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class SpawnGameObjectAtOwnPosition : MonoBehaviour
	{
		public bool MakeChild;
		public GameObject GameObject;

		public void Spawn()
		{
			var obj = Instantiate(GameObject, transform.position, Quaternion.identity);

			if (MakeChild)
				obj.transform.parent = transform;
		}
	}
}
