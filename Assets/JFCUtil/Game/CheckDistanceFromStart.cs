﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class CheckDistanceFromStart : MonoBehaviour
	{
		public float DistanceFromStart
		{
			get { return Vector3.Distance(transform.position, _startPoint); }
		}
		private Vector2 _startPoint;

		private void Awake()
		{
			_startPoint = transform.position;
		}
	}
}
