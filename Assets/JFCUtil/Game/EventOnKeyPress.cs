﻿using UnityEngine;
using UnityEngine.Events;

namespace JFCUtil.Game
{
	public class EventOnKeyPress : MonoBehaviour
	{
		public UnityEvent OnPress;
		public KeyCode KeyCode;

		private void Update()
		{
			if(Input.GetKeyDown(KeyCode) && OnPress != null)
				OnPress.Invoke();
		}
	}
}
