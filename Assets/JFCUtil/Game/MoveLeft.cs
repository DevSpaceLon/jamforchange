﻿using UnityEngine;

namespace JFCUtil.Game
{
	public class MoveLeft : MonoBehaviour 
	{
		public void Move()
		{
			transform.Translate(new Vector2(-1f, 0f));
		}
	}
}
