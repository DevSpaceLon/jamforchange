﻿using System.Collections;
using JFCUtil.Display;
using JFCUtil.Game;
using UnityEngine;

namespace JFCUtil.Main
{
	public class GameMachine : MonoBehaviour
	{
		public GameConfig[] Games;
		public GameConfig CurrentGame;

		private IGameClass _gameClass;
		private GameObject _titleObject;
		private GameObject _gameObject;
		private GameObject _scorePrefab;

		private void Awake()
		{
			StartNextGame();
		}

		private void StartNextGame()
		{
			Camera.main.backgroundColor = CurrentGame.MainColor;
			_titleObject = Instantiate(CurrentGame.TitleObject);
			StartCoroutine(RunGameFromTitleThenShowScore());
		}

		private IEnumerator RunGameFromTitleThenShowScore()
		{
			yield return new WaitForSeconds(2f);
			_gameObject = Instantiate(CurrentGame.GamePrefab);
			_gameClass = _gameObject.GetComponent<IGameClass>();
			
			var timers = FindObjectsOfType<Timer>();
			foreach (var timer in timers)
			{
				timer.SetTimer(CurrentGame.GameLength);
			}
			
			Destroy(_titleObject);
			yield return new WaitForSeconds(CurrentGame.GameLength);
			
			var score = _gameClass.GetScoreOutOfOneHundred();
			_scorePrefab = Instantiate(CurrentGame.ScorePrefab);
			
			yield return new WaitForEndOfFrame();

			var abstractScoreScreen = _scorePrefab.GetComponent<AbstractScoreScreen>();
			var currentSetting = 0;
			
			abstractScoreScreen.SetScreen(CurrentGame.MainColor, CurrentGame.SecondaryColor, CurrentGame.TertiaryColor, currentSetting);

			while (currentSetting < score)
			{
				currentSetting++;
				abstractScoreScreen.SetScreen(CurrentGame.MainColor, CurrentGame.SecondaryColor, CurrentGame.TertiaryColor, currentSetting);
				yield return new WaitForEndOfFrame();
			}
			
			Destroy(_gameObject);
			
			yield return new WaitForSeconds(2f);
			Destroy(_scorePrefab);

			CurrentGame = Games[Random.Range(0, Games.Length)];

			DoCleanup();
			
			StartNextGame();
		}

		private void DoCleanup()
		{
			var cleans = FindObjectsOfType<Cleanup>();

			for (var index = cleans.Length-1; index >= 0 ; index--)
			{
				Destroy(cleans[index].gameObject);
			}
		}
	}
}
