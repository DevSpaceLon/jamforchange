﻿using UnityEngine;
using UnityEngine.Events;

namespace JFCUtil.Events
{
	public class GameEventOnTimer : MonoBehaviour
	{
		public bool Repeating = true;
		public float Interval;
		public UnityEvent Event;

		private float _elapsed;

		private void Update()
		{
			_elapsed += Time.deltaTime;

			if (_elapsed < Interval) 
				return;
			
			_elapsed = 0f;
			Event.Invoke();
			
			if (!Repeating) 
				enabled = false;
		}
	}
}
